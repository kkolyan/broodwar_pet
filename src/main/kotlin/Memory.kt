import bwapi.Unit

class Memory {
    val returnToWorkAfterCurrent = ArrayDeque<Unit>()
    val bases = mutableListOf<Base>()
    val requests = mutableSetOf<ResourceRequest>()
    val unitComplete = mutableSetOf<UnitCompleteExpectation>()
}
