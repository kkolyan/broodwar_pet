import bwapi.TilePosition

sealed class CompletionSource {
    data class Position(val position: TilePosition) : CompletionSource()
    data class Unit(val unit: bwapi.Unit) : CompletionSource()
}
