import bwapi.Unit

class Base () {
    val zealots = mutableSetOf<Unit>()
    val workers = mutableSetOf<Unit>()
    var nexus : Unit? = null
    val gateways = mutableSetOf<Unit>()
}