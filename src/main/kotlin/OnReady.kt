fun interface OnReady {
    fun onReady(source: Unit, ready: Unit)
}