import bwapi.Game

interface Task {
    fun onStart(game: Game, memory: Memory)
    fun onFrame(game: Game, memory: Memory)
}