import bwapi.Unit

class UnitCompleteExpectation(val desc: String, val source: CompletionSource) {
    var completed: Unit? = null
}