package processes

import Memory
import Priority
import ResourceRequest
import Task
import bwapi.Game
import bwapi.UnitType
import info
import tags
import kotlin.math.min

class KeepSuppliesProcess : Task {
    private var newPylon: ResourceRequest? = null
    private var newPylonSuppressTill: Int? = null

    override fun onStart(game: Game, memory: Memory) {
    }

    override fun onFrame(game: Game, memory: Memory) {
        val inProgress = game.self().units
            .filter { it.isConstructing && it.type == UnitType.Protoss_Pylon }
            .sumOf { it.type.supplyProvided() }
        if (newPylonSuppressTill?.let { it < game.elapsedTime() } == true) {
            newPylonSuppressTill = null
        }
        if (game.self().supplyTotal() + inProgress < min(game.self().supplyUsed() * 1.2, 400.0)
            && newPylon == null
            && newPylonSuppressTill == null
        ) {
            println("critical supply level, requesting resources for new supply unit")
            val request = ResourceRequest("new pylon", Priority.Supply, UnitType.Protoss_Pylon)
            memory.requests.add(request)
            newPylon = request
        }
        if (newPylon?.agreed == true) {
            println("new supply unit resources agreed")
            newPylon!!.consumed = true
            newPylon = null

            val where = game.getBuildLocation(
                UnitType.Protoss_Pylon,
                memory.bases.first().nexus!!.tilePosition
            )
            val worker = memory.bases.first().workers.first()
            worker.tags.add("building pylon")
            worker.build(UnitType.Protoss_Pylon, where)
            memory.returnToWorkAfterCurrent.add(worker)
            newPylonSuppressTill = game.elapsedTime() + Consts.PylonSupressionTimeout
            println("worker sent to build ${worker.info}")
        }
    }
}