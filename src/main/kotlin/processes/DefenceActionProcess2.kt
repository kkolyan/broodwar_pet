package processes

import Consts
import Memory
import Task
import bwapi.Game
import bwapi.Unit
import bwapi.UnitType
import info

class DefenceActionProcess2 : Task {
    private val enemyHandlers = mutableMapOf<Unit, MutableSet<Unit>>()
    private val zealotTargets = mutableMapOf<Unit, Unit>()
    override fun onStart(game: Game, memory: Memory) {
    }

    override fun onFrame(game: Game, memory: Memory) {
        zealotTargets.values.removeIf { !it.exists() }
        zealotTargets.keys.removeIf { !it.exists() }
        enemyHandlers.keys.removeIf { !it.exists() }
        val zealots = game.self().units
            .filter { it.type == UnitType.Protoss_Zealot }
            .filter { !zealotTargets.containsKey(it) }
            .toMutableList()
        game.allUnits
            .filter { it.player.isEnemy(game.self()) }
            .forEach { enemy ->
                val handlers = enemyHandlers
                    .computeIfAbsent(enemy) {
                        println("enemy registered: ${enemy.info}")
                        mutableSetOf() }
                handlers.removeIf { !it.exists() }
                while (handlers.size < Consts.MaxZealotsPerEnemy && zealots.isNotEmpty()) {
                    val zealot = zealots.removeFirst()
                    zealotTargets[zealot] = enemy
                    handlers += zealot
                    zealot.attack(enemy)
                }
            }
    }
}