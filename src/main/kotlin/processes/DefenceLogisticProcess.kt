package processes

import CompletionSource
import Memory
import ResourceRequest
import Task
import UnitCompleteExpectation
import bwapi.Game
import bwapi.UnitType

class DefenceLogisticProcess : Task {
    private var gatewayResourceRequest: ResourceRequest? = null
    private var gatewayConstructionWaiting: UnitCompleteExpectation? = null
    private var zealotResourcesRequests = mutableSetOf<ResourceRequest>()
    private var zealotTrainingWaiting = mutableSetOf<UnitCompleteExpectation>()

    override fun onStart(game: Game, memory: Memory) {
    }

    override fun onFrame(game: Game, memory: Memory) {
        val base = memory.bases.singleOrNull() ?: return

        if (base.workers.size > 9 && base.zealots.size < 240) {

            zealotTrainingWaiting
                .removeIf {
                    if (it.completed != null) {
                        base.zealots += it.completed!!
                        true
                    } else false
                }

            zealotResourcesRequests
                .removeIf { request ->
                    if (request.agreed) {
                        base.gateways
                            .find { it.trainingQueueCount < 1 }
                            ?.let { gateway ->
                                if (gateway.train(UnitType.Protoss_Zealot)) {
                                    request.consumed = true;
                                    val waiting = UnitCompleteExpectation("new zealot", CompletionSource.Unit(gateway))
                                    memory.unitComplete += waiting
                                    zealotTrainingWaiting += waiting
                                    true
                                } else false
                            }
                            ?: false
                    } else false
                }

            base.gateways
                .filter { it.trainingQueueCount < 1 }
                .drop(zealotResourcesRequests.size)
                .forEach {
                    val request = ResourceRequest("new zealot", Priority.Defence, UnitType.Protoss_Zealot)
                    memory.requests += request
                    zealotResourcesRequests += request
                }

            if (gatewayConstructionWaiting?.completed != null) {
                val gateway = gatewayConstructionWaiting!!.completed!!
                base.gateways += gateway
                gatewayConstructionWaiting = null
            }

            if (gatewayResourceRequest?.let { it.agreed } == true) {
                println("gateway resources agreed")

                val where = game.getBuildLocation(
                    UnitType.Protoss_Gateway,
                    base.nexus!!.tilePosition
                )

                val worker = base.workers.last()

                worker.build(UnitType.Protoss_Gateway, where)
                gatewayResourceRequest!!.consumed = true
                gatewayResourceRequest = null
                gatewayConstructionWaiting = UnitCompleteExpectation("new gateway", CompletionSource.Position(where))
                memory.unitComplete += gatewayConstructionWaiting!!

                memory.returnToWorkAfterCurrent += worker

            }
            if ((base.gateways.size < 1 || (base.gateways.size < 2 && game.self().minerals() > 300)) && gatewayResourceRequest == null && gatewayConstructionWaiting == null) {
                println("requesting resources for new gateway")
                gatewayResourceRequest = ResourceRequest("new gateway", Priority.Defence, UnitType.Protoss_Gateway)
                memory.requests += gatewayResourceRequest!!
            }
        }
    }
}