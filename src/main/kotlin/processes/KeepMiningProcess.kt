package processes

import CompletionSource
import Consts
import Memory
import Priority
import ResourceRequest
import Task
import UnitCompleteExpectation
import bwapi.Game
import bwapi.Unit
import info

class KeepMiningProcess : Task {

    private val newWorkerTrainingWaiting = mutableSetOf<UnitCompleteExpectation>()
    private var newWorkerResourcesRequest: ResourceRequest? = null

    override fun onStart(game: Game, memory: Memory) {
        game.self().units
            .filter { it.type.isWorker }
            .forEach { unit ->

                goWork(unit, game)
            }
    }

    private fun goWork(unit: Unit, game: Game) {
        game.minerals
            .minByOrNull { it.position.getDistance(unit.position) }
            ?.let {
                unit.gather(it, true)
            }
            ?: println("no ore chunks found for ${unit.info}")
    }

    override fun onFrame(game: Game, memory: Memory) {
        while (memory.returnToWorkAfterCurrent.isNotEmpty()) {
            goWork(memory.returnToWorkAfterCurrent.removeFirst(), game)
        }
        newWorkerTrainingWaiting.removeIf {
            if (it.completed != null) {
                println("worker training completed - sending to ore ${it.completed!!.info}")
                goWork(it.completed!!, game)
                memory.bases.first().workers.add(it.completed!!)
                true
            } else {
                false
            }
        }
        for (base in memory.bases) {
            if (base.nexus == null) {
                continue
            }
            if (newWorkerResourcesRequest != null && newWorkerResourcesRequest!!.agreed) {
                newWorkerResourcesRequest!!.consumed = true
                newWorkerResourcesRequest = null

                println("new worker training agreed, start training at ${base.nexus!!.info}")

                base.nexus!!.train(Consts.WorkerType)
                val expectation = UnitCompleteExpectation(
                    "worker for minerals",
                    CompletionSource.Unit(base.nexus!!)
                )
                newWorkerTrainingWaiting += expectation
                memory.unitComplete += expectation
            }
            if (base.workers.size < Consts.RequiredWorkersPerBase
                && base.nexus!!.trainingQueue.size < 1
                && newWorkerResourcesRequest == null
            ) {
                println("requesting resources for new worker")
                val request = ResourceRequest(
                    "new worker",
                    Priority.Mining,
                    Consts.WorkerType
                )
                newWorkerResourcesRequest = request
                memory.requests.add(request)
            }
        }
    }
}