package processes

import Memory
import Task
import bwapi.Color
import bwapi.Game
import kotlin.math.max

class IncomeCalculator : Task {
    private var prevGathered = 0
    private var delta = IntArray(2500)
    private var deltaIndex = 0
    private var miningRate = DoubleArray(1024)
    private var miningRateSize = 0
    private val buffer = DoubleArray(2000)

    override fun onStart(game: Game, memory: Memory) {
    }

    override fun onFrame(game: Game, memory: Memory) {
        val gathered = game.self().gatheredMinerals()
        delta[deltaIndex] = gathered - prevGathered
        deltaIndex = (deltaIndex + 1) % delta.size
        prevGathered = gathered

        miningRate[miningRateSize] = delta.average()
        miningRateSize++
        if (miningRateSize >= miningRate.size) {
            miningRate = DoubleArray(miningRate.size * 2) { miningRate.getOrNull(it) ?: 0.0 }
        }


        val n = max(1, miningRateSize / 1000)
        for (i in 0..<(miningRateSize / n)) {
            var sum = 0.0
            for (j in 0..<n) {
                sum += miningRate[i * n + j]
            }
            buffer[i] = 1.0 * sum / n
        }


        val step = 640.0 / miningRateSize * n
        val max = miningRate.max()
        val height = 200
        for (i in 0..<(miningRateSize / n)) {
            val value = buffer[i]
            game.drawDotScreen((i * step).toInt(), (height * (1.0 - value / max)).toInt(), Color.Blue)
        }
    }
}