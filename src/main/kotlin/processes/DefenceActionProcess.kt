package processes

import Consts
import Memory
import Task
import bwapi.Game
import bwapi.Order
import bwapi.Unit
import bwapi.UnitType
import info

class DefenceActionProcess : Task {
    private val enemyHandlers = mutableMapOf<Unit, MutableSet<Unit>>()
    private val zealotTargets = mutableMapOf<Unit, Unit>()

    private val involved = mutableSetOf<Unit>()
    override fun onStart(game: Game, memory: Memory) {
    }

    private var slowed = false
    override fun onFrame(game: Game, memory: Memory) {
        val enemies = game.allUnits
            .filter { it.player.isEnemy(game.self()) }
            .toMutableList()

        if (enemies.isEmpty()) {
            return
        }

        if (!slowed) {
            game.setLocalSpeed(1);
            slowed = true
        }

        game.self().units
            .filter { it.type == UnitType.Protoss_Zealot }
            .filter { involved.add(it) }
            .forEach { defender ->
                val target = enemies.minByOrNull { it.position.getDistance(defender.position) }
                if (target != null) {
                    defender.attack(target.position)
                }
            }
    }
}