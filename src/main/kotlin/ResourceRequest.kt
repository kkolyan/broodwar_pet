import bwapi.UnitType

class ResourceRequest(
    val desc: String,
    val priority: Priority,
    type: UnitType
) {
    val minerals = type.mineralPrice()
    val gas = type.gasPrice()
    val supply = type.supplyRequired()

    var agreed: Boolean = false
    var consumed: Boolean = false
}