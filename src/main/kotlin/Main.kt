import bwapi.*
import bwapi.Unit
import processes.DefenceActionProcess
import processes.DefenceLogisticProcess
import processes.KeepMiningProcess
import processes.KeepSuppliesProcess
import java.util.WeakHashMap
import kotlin.math.max

object Bot : DefaultBWListener() {
    override fun onStart() {
        val base = Base()
        base.workers.addAll(game.self().units.filter { it.type.isWorker })
        base.nexus = game.self().units.find { it.type == UnitType.Protoss_Nexus }
        memory.bases.add(base)

        tasks.forEach { it.onStart(game, memory) }

        game.setLocalSpeed(0)
    }

    override fun onEnd(isWinner: Boolean) {

    }

    override fun onSendText(text: String) {

    }

    override fun onReceiveText(player: Player, text: String) {

    }

    override fun onPlayerLeft(player: Player) {

    }

    override fun onNukeDetect(target: Position) {

    }

    override fun onUnitDiscover(unit: Unit) {

    }

    override fun onUnitEvade(unit: Unit) {

    }

    override fun onUnitShow(unit: Unit) {

    }

    override fun onUnitHide(unit: Unit) {

    }

    override fun onUnitMorph(unit: Unit) {

    }

    override fun onUnitRenegade(unit: Unit) {

    }

    override fun onSaveGame(gameName: String?) {

    }

    override fun onPlayerDropped(player: Player) {

    }

    override fun onUnitComplete(unit: Unit) {
        if (unit.player != game.self()) {
            return
        }
        println("unit complete: ${unit.info}")
        memory.unitComplete.removeIf {
            val matches = when(it.source) {
                is CompletionSource.Position -> {
                    it.source.position.getDistance(unit.tilePosition) < 4
                }
                is CompletionSource.Unit -> it.source.unit.tilePosition.getDistance(unit.tilePosition) < 4
            }
            if (matches) {
                it.completed = unit
            }
            matches
        }
    }

    override fun onFrame() {
        val elapsedTime = game.elapsedTime()
        val seconds = elapsedTime % 60
        val minutes = elapsedTime / 60 % 60;
        val hours = elapsedTime / 3600
        game.drawTextScreen(20, 20, "%02d:%02d:%02d".format(hours, minutes, seconds))
        for (task in tasks) {
            task.onFrame(game, memory)
        }

        var minerals = game.self().minerals()
        var gas = game.self().gas()
        var supply = game.self().supplyTotal() - game.self().supplyUsed()

        memory.requests.removeIf { it.consumed }

        for (request in memory.requests.sortedBy { it.priority }) {
            if (request.minerals <= minerals && request.gas <= gas && request.supply <= supply) {
                request.agreed = true
            }
            minerals -= request.minerals
            supply = max(0, supply - request.supply)
            gas = max(0, gas - request.gas)
        }
    }

    override fun onUnitCreate(unit: Unit) {

    }

    override fun onUnitDestroy(unit: Unit) {

    }
}

data class UnitKey(
    val player: String,
    val group: String,
    val id: String
)

private val unitNames = WeakHashMap<Unit, UnitKey>()

val Unit.key
    get() : UnitKey? {
        return unitNames[this]
    }

val Unit.info
    get(): String {
        return "$type($id $tags)"
    }

private val unitTags = WeakHashMap<Unit, MutableSet<String>>()

val Unit.tags
    get(): MutableSet<String> {
        return unitTags.computeIfAbsent(this) { mutableSetOf() }
    }

lateinit var client: BWClient

val game: Game
    get() = client.game


var tasks = mutableSetOf<Task>(
    KeepMiningProcess(),
    KeepSuppliesProcess(),
    DefenceLogisticProcess(),
    DefenceActionProcess()
//    IncomeCalculator(),
)
val memory = Memory()

fun main(args: Array<String>) {
    client = BWClient(Bot)
    client.startGame(BWClientConfiguration().withAutoContinue(true))
}