import bwapi.UnitType

object Consts {
    const val MaxZealotsPerEnemy = 3
    const val PylonSupressionTimeout = 5
    const val RequiredWorkersPerBase = 30 // measured number
    val WorkerType = UnitType.Protoss_Probe
}